<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'aar_intranet2017');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '1234');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'i!e;>o_).=9S*=QT!W$mBYzPRx`Wpa+h:[i&*TpEse=;.mE~u2[ $y-88IKEge8A');
define('SECURE_AUTH_KEY',  '6w~EX%JnDNhpsJWP!H2Y]8_jV.RbT9.goZgu{-s{4A1r!+4D6xhF v[e{KL=?&D*');
define('LOGGED_IN_KEY',    'o?-ZY3~4Ap*wN;WV^BXq.U@KjqX-toZ)Q(R5_3(Z-S-Qj6kdX*-Uew>L!K~a>i/v');
define('NONCE_KEY',        '<UXx;8z1>)yzVzs</|FuUFCr#COjLAl!-lry*I.ywd$.%+1;n9F/9d -cj,s5};e');
define('AUTH_SALT',        'SYc|(sGC%-,6g^W~Avbr_,CsX4^4tXj1S0)@q)|NSYvFChmc7}~/`is +RDDM(XL');
define('SECURE_AUTH_SALT', '{>PoS;L>os3z[J2@@t{1j4~YA<$jX~7dt W^IpX78-VBB)Uo,fR1Q}`N{hO4w-cf');
define('LOGGED_IN_SALT',   'TV&:;aopOm}Eb1eb*7y*L3yXek*7E-`YFtQQf`br(m(3jAb;d3n >}j9N[KKnW@f');
define('NONCE_SALT',       'fU/6qUHc]r0)WHK(^6ULHZlF1NoE._rv&_VuarE4%9~nO[h5WFpJ~,MN7pI??M(|');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'aar_intranet_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */
define('FS_METHOD','direct');

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
