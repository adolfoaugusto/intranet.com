<?php 
add_filter('pre_get_posts', 'my_query_post_type');
function my_query_post_type($query){
	if ( $query->is_main_query() && !is_admin() ) {
		if ( is_category() || is_tag() ) {
			$query->set( 'post_type', array('post', 'resources' ) );
		}
	}
}

/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function twentyseventeen_posted_on() {

	if ('resources' === get_post_type() && function_exists( 'coauthors_posts_links' ) ) {
		$byline = $author = "by ".coauthors_posts_links( null, null, null, null, false );

	}else{
		// Get the author name; wrap it in a link.
		$byline = sprintf(
			/* translators: %s: post author */
			__( 'by %s', 'twentyseventeen' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . get_the_author() . '</a></span>'
		);

	}
	// Finally, let's write all of this to the page.
	echo '<span class="posted-on">' . twentyseventeen_time_link() . '</span><span class="byline"> ' . $byline . '</span>';
}